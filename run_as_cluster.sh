#!/bin/bash

# load our software environment
source activate qiime

# load the cluster's qiime install (becuase we cant seem to install python2 and 3 together) :(
module load qiime
nohup snakemake -j 100 --cluster-config cluster.json --cluster 'sbatch -c {threads} -t {cluster.time} --mem {cluster.mem} -o misc-log.log' &

