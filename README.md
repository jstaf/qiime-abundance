QIIME pipeline
========================

We use FastQC and MultiQC to get a good look at our sequencing samples before we do anything (if something crops up here, there are tools to cleanup our samples!). Once our samples look good, we use QIIME to classify and count the number of reads in each OTU. We then use edgeR (haven't done this part yet!) to find differences between sample categories. 

Because running/installing all of this stuff is complicated, use a couple Python tools to make things easier:

* Snakemake automates this analysis pipeline start-to-finish. We essentially tell it how to produce each output file from each input file, and then it handles running things for us in the proper order with the proper commands.
* Anaconda/Miniconda is both a Python distribution and a "package manager" that installs software for us in a super-reproducible manner using software installation "recipes" from Bioconda. 

This pipeline/guide works on either OSX or Linux (most bioinformatics tools don't work on Windows unfortunately).

# Installing the software

There is a little bit of added complexity here because QIIME is a Python 2 program, and Snakemake and all of the other software runs on Python 3 (they don't play nice together). 

## To install QIIME (do this first!)

* Download "Miniconda" (Python 2.7) from the link here: [http://conda.pydata.org/miniconda.html](http://conda.pydata.org/miniconda.html). This is a minimal install of Python 2. 
* Open up Terminal, and "cd" to the directory where Miniconda was downloaded to (should be `~/Downloads`)
* Install Miniconda 2.7 by typing `bash Miniconda2-latest-MacOSX-x86_64.sh` (choose all the default options)
* Install QIIME on that copy of miniconda by typing `conda install -c bioconda qiime matplotlib=1.4.3 mock nose`
* If you've done things correctly, the command `pick_open_reference_otus.py --version` should return something like "version blah blah blah" (the important thing is that you see a version number and not "command not found")

## To install the rest of the stuff

* We are going to install the big, fat version of Python 3 that comes with all of the extra stuff. Download the Python 3.5 vesion of Anaconda from [https://www.continuum.io/downloads]([https://www.continuum.io/downloads). This one is nice enough to come with a graphical installer (pick all of the default options).
* Once Anaconda is installed, you can install all of the other software with `conda install -c bioconda snakemake fastqc multiqc` and then `conda install graphviz`
* If you feel like testing to make sure things got installed correctly, you can type `fastqc --version`, `snakemake --version`, `multiqc --version`. As long as it doesn't say "command not found", you're set.

To install the R packages you need, either open up R or RStudio (install these on your computer if you don't have them already!). In the R console type (if it asks you to create a "personal library", just pick/type "yes"):

```{r}
install.packages(c("stringr", "plyr", "reshape2", "ggplot2", 
	"RColorBrewer", "magrittr", "tidyr", "Cairo"))
source("https://bioconductor.org/biocLite.R")
biocLite()
biocLite(c("edgeR", "biomformat"))
```

# Running the analysis

To get ready to run things, simply drop your .fastq files in the `fastq` folder and add the correct sample names/sample groups to the `sample_info.tsv` file (you can use the existing `sample_info.tsv` file as an example). Once that's done, you can open up Terminal and `cd` to the directory with the `Snakefile` in it.

To run the analysis, type `snakemake --cores N`, where `N` is the number of cores in your computer (for running things in parallel). If you don't know how many cores you have, you can get it with `grep -c proc /proc/cpuinfo`. It'll give progress reports with how far along it is (like "14 of 20 steps (70%) done"). To stop things, use `ctrl-c` or close the Terminal window (Snakemake is smart enough to figure out what's already been done and what hasn't, so it'll just pick up from where it left off last time the next time you run things). 

Important output files:

* `otu_table.csv` - This is the table of raw counts for each OTU.
* `div/index.html` - Open this in a web browser (just double-click on it) and it will take you to a "dashboard" with most of the plots from a given run.
* Various `.png` files - Some summary plots produced in R by the `plot_diversity.R` script.

-------------------

To do a "dry run" (do nothing besides print out what commands `snakemake` is going to run for you):
`snakemake -np`

To clean up all intermediate files/output (so you can rerun things): `snakemake clean`

To run a specific rule/step, you can type `snakemake ruleName` (so `snakemake multiqc` would run all of the QC reports). To force it to rerun a step, you can add the `--force` option (`snakemake --force --cores 8 multiqc`, for instance).

To make a pretty "workflow diagram" of all the steps Snakemake has done/will do: `snakemake dag` (check out `dag.svg` for the resulting diagram).

To tweak how the pipeline is run, open up the Snakefile and edit the "shell" bit for each rule. The only thing I'd say you "have" to edit is the `-e 845` bit in the `calc_diversity` rule. The "845" bit is the minimum number of reads for a sample to be included in the analysis. All of the other samples get "down-sampled" to this number for the purpose of the plots you get from the `div/index.html` file.


# Running things on the CAC cluster

* Get an account by following the instructions here: [http://cac.queensu.ca/accounts/sign-up-procedure/](http://cac.queensu.ca/accounts/sign-up-procedure/)
* Upload the folder with your data using the instructions here: [http://cac.queensu.ca/wiki/index.php/HowTo:UploadingFiles](http://cac.queensu.ca/wiki/index.php/HowTo:UploadingFiles). All of the software you need is preinstalled.
* Logon to the cluster and submit the pipeline to the scheduler with `qsub run_as_cluster.sh`
* Check the status of things by typing `qstat`. You can cancel everything by typing `qdel -u yourUserName`




