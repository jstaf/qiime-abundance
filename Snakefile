# Analysis pipeline for Prameet Sheth
# Jeff Stafford - CAC

import glob

# generate a list of unique sample and fastq names that we will be analyzing
SAMPLES, READS = glob_wildcards('fastq/{sample}_R{read}_001.fastq',
								files = glob.glob('fastq/*.fastq'))
FASTQS = expand('{sample}_R{read}_001', sample=SAMPLES, read=set(READS))
SAMPLES = expand('{sample}_R{read}_001', sample=set(SAMPLES), read=[1])

# which rules should be executed locally...
# (i.e. not submitted as a job to the cluster)
localrules: all, clean, multiqc, dag, validate_mappings, tar_output

shell.prefix('PATH=/opt/R/microsoft-3.3.1/microsoft-r/3.3/lib64/R/bin/:$PATH ')

# run pipeline to completion
# input files for this rule are the "expected" final output files
rule all:
	input:	'results.tar.gz'


# clean up for new run
rule clean:
	shell:
		'''
		rm -f *.o* logs/* fastq/*.html fastq/*.zip div_complete *.png *.svg otu_table.csv *.o* *.out
		rm -rf fastq_join/ fastq_qiime/ vmf_map/ otus/ div/ multiqc/
		'''


# get diagnostic info on fastq files
rule fastqc:
	input:	'fastq/{sample}.fastq'
	output:	'fastq/{sample}_fastqc.html'
	log:	'fastq/{sample}-fastqc.log'
	shell:	'fastqc {input} &> {log}'


# aggregate qc data
rule multiqc:
	input:	expand('fastq/{fastq}_fastqc.html', fastq=FASTQS)
	output: 'multiqc/multiqc_report.html'
	shell:	'multiqc -o multiqc fastq'


# to validate your sample_info.tsv file
rule validate_mappings:
	input:	'sample_info.tsv'
	shell:	'validate_mapping_file.py -o vmf_map -m sample_info.tsv -p -b'


# create a pretty "workflow diagram"
rule dag:
	shell:	'snakemake --dag | dot -Tsvg > dag.svg'


# join all paired ends in one go for qiime
rule join_paired_ends:
	output:	expand('fastq_join/{sample}/fastqjoin.join.fastq', sample=SAMPLES)
	shell:	'multiple_join_paired_ends.py -i fastq -o fastq_join'


# have qiime make the special fasta file used for otu calling
rule make_qiime_fasta:
	input:	expand('fastq_join/{sample}/fastqjoin.join.fastq', sample=SAMPLES)
	output:	'fastq_qiime/seqs.fna'
	shell:
		'''
		multiple_split_libraries_fastq.py --barcode_indicator 'not-barcoded' --demultiplexing_method sampleid_by_file \
			--include_input_dir_path --remove_filepath_in_name -i fastq_join -o $(dirname {output}) 
		'''


# perform open reference otu picking against greengenes
rule pick_otus:
	input:	'fastq_qiime/seqs.fna'
	output:	'otus/otu_table_mc2_w_tax_no_pynast_failures.biom'
	threads: 24
	shell:	'pick_open_reference_otus.py -f -o $(dirname {output}) -i {input} -p qiime_params.txt -a -O {threads}'


# run core diversity analysis pipeline
rule calc_diversity:
	input:	
		otus = 'otus/otu_table_mc2_w_tax_no_pynast_failures.biom',
		smpinfo = 'sample_info.tsv'
	output:	'div_complete'
	threads: 6
	shell:	'core_diversity_analyses.py -o div -i {input.otus} -m {input.smpinfo} -t otus/rep_set.tre -e 15000 -a -O {threads} && touch {output}'


# qiime's differential abundance calcs
rule qiime_diff_abundance:
	input:	
		otus='otus/otu_table_mc2_w_tax_no_pynast_failures.biom',
		smpinfo='sample_info_deseq.tsv'
	#output: 'qdiff/'
	#shell:	'differential_abundance.py -i {input.otus} -o {output} -m {input.smpinfo} -a metagenomeSeq_fitZIG -c Condition -x pre-flow -y double+'
	shell:	'differential_abundance.py -i {input.otus} -o qdiff/deseq.tsv -m {input.smpinfo} -a DESeq2_nbinom -d -c Condition -x pre_flow -y double_plus'


# convert otu tables to something more user-friendly
rule make_otu_csv:
	input:	'otus/otu_table_mc2_w_tax_no_pynast_failures.biom'
	output:	'otu_table.csv'
	shell:	'Rscript extract_counts.R {input} {output}'


# produce a couple summary diversity plots
rule plot_diversity:
	input:	
		otus='otu_table.csv',
		smpinfo='sample_info.tsv'
	output:	'div_sample_scaled.png'
	shell:	'Rscript plot_diversity.R {input.otus} {input.smpinfo}'


# compress output into nice tar archive for email
rule tar_output:
	input:
		'div_complete',
		'multiqc/multiqc_report.html',
		'div_sample_scaled.png'
	output: 'results.tar.gz'
	shell:	'GZIP=-9 tar -czvf {output} div/ *.png otu_table.csv multiqc/multiqc_report.html'


